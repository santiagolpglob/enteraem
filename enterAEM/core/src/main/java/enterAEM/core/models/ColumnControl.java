package enterAEM.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.collections.IteratorUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ColumnControl {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Required
	@ScriptVariable
	private PageManager pageManager;

	@Required
	@ScriptVariable
	private Page resourcePage;

	@Required
	@ScriptVariable
	private Resource resource;

	@Required
	@ScriptVariable
	private Node currentNode;

	@Required
	@ScriptVariable
	private ResourceResolver resolver;

	@ValueMapValue
	public String desktopColumns;

	@ValueMapValue
	public String tabletColumns;

	@ValueMapValue
	public String parentpath;

	@ValueMapValue
	public String maxcards;

	private List<Column> col;

	@PostConstruct
	public void init() {

		Page pageFromPath;

		log.info("ParentPath: " + parentpath + " Maxcards: " + maxcards);
		log.info("Column Control *** INIT ***");

		arrangeColumns();

		if (parentpath != null) {

			removeAllChildren();
			pageFromPath = pageManager.getContainingPage(parentpath);
			log.info("**** parentPage path: " + pageFromPath.getPath());

			if (pageFromPath.hasContent()) {
				log.info("ABOUT TO SET CARDS");

				setCards(pageFromPath);
			}
		}
	}

	public void setCards(Page parentPage) {

		log.info("**** INSIDE getChildren fn");
		log.info("**** resource PATH: " + resource.getPath());

		Iterator<Page> childrenPageIter = parentPage.listChildren();
		List<Page> childrenPageList = IteratorUtils.toList(childrenPageIter);
		int topCards = childrenPageList.size() < Integer.parseInt(maxcards) ? childrenPageList.size()
				: Integer.parseInt(maxcards);

		int desktopColumnsInt = Integer.parseInt(desktopColumns);

		log.info("This path has this many pages: " + childrenPageList.size());

		Resource newPar = null;
		HashMap<String, Object> parProperties = new HashMap<>(), cardProperties = new HashMap<>();
		parProperties.put("sling:resourceType", "wcm/foundation/components/parsys");
		cardProperties.put("sling:resourceType", "enterAEM/components/content/newscard");

		for (int i = 0; i < desktopColumnsInt; i++) {
			try {
				newPar = null;
				newPar = ResourceUtil.getOrCreateResource(resolver, resource.getPath().concat("/par_" + i),
						parProperties, null, true);

				for (int p = i; p < topCards; p += desktopColumnsInt) {
					cardProperties.put("path", childrenPageList.get(p).getPath());

					ResourceUtil.getOrCreateResource(resolver,
							newPar.getPath().concat("/" + ResourceUtil.createUniqueChildName(newPar, "card")),
							cardProperties, null, true);
				}

			} catch (PersistenceException e) {
				e.printStackTrace();
			}
		}
	}

	private void removeAllChildren() {
		try {
			NodeIterator currentNodeIter = currentNode.getNodes("par_*");

			while (currentNodeIter.hasNext()) {
				Node n = currentNodeIter.nextNode();
				log.info("Node to be removed: " + n.getPath());
				n.remove();
			}

		} catch (RepositoryException e) {
			log.error("**** Par Node Deleting Exception");
			e.printStackTrace();
		}
	}

	private void arrangeColumns() {
		col = new ArrayList<Column>();

		if (desktopColumns != null || tabletColumns != null) {

			int dktpCols = Integer.parseInt(desktopColumns);
			int tabCols = Integer.parseInt(tabletColumns);

			log.error("Column Control **** Deswktop cols ***" + dktpCols);
			log.error("Column Control **** Tablet Cols ***" + tabCols);
			log.error("FooterGlobal **** smval ***" + 12 / tabCols);

			for (int i = 0; i < dktpCols; i++) {
				Column item = new Column();
				item.setCount(i);
				item.setDeskVal(12 / dktpCols);
				item.setTabVal(12 / tabCols);
				log.error("Column Control **" + item.toString());
				col.add(item);
			}
		}
		setCol(col);
	}

	public List<Column> getCol() {
		return col;
	}

	public void setCol(List<Column> col) {
		this.col = col;
	}
}
