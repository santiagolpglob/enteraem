package enterAEM.core.models;

import java.util.Map;

public class Column {

	private int count;
	private int val;
	private int deskVal;
	private int tabVal;
	private Map<String, String> clssAttr1;
	private Map<String, String> clssAttr2;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}

	public int getDeskVal() {
		return deskVal;
	}

	public void setDeskVal(int deskVal) {
		this.deskVal = deskVal;
	}

	public int getTabVal() {
		return tabVal;
	}

	public void setTabVal(int tabVal) {
		this.tabVal = tabVal;
	}

	public Map<String, String> getClssAttr1() {
		return clssAttr1;
	}

	public void setClssAttr1(Map<String, String> clssAttr1) {
		this.clssAttr1 = clssAttr1;
	}

	public Map<String, String> getClssAttr2() {
		return clssAttr2;
	}

	public void setClssAttr2(Map<String, String> clssAttr2) {
		this.clssAttr2 = clssAttr2;
	}

}
