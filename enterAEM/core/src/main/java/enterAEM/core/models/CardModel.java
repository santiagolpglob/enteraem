package enterAEM.core.models;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.wcm.core.components.models.Image;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import enterAEM.core.ArticleAuthorPojo;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

@Model(adaptables= {SlingHttpServletRequest.class},
resourceType= {CardModel.RESOURCETYPE},
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class CardModel {

	protected static final String RESOURCETYPE = "enterAEM/components/content/newscard";
	private static final String IMAGE_LOCATION = "root/image"; 
	private static final Logger log = LoggerFactory.getLogger(CardModel.class);
	@ValueMapValue
	private String path;
	
	@ScriptVariable
	@Required
	private PageManager pageManager;
	
	private Page pageForCard;
	
	/***
	 * Boolean for checking existing articleAuthor Component in current page
	 */
	private Boolean authorExists = false;
	@Self
	@Required
	private SlingHttpServletRequest request;
	
	@OSGiService
	private ModelFactory modelFactory;
	
	private String sinAutorDefault="Sin autor";
	
	private ArticleAuthorPojo author;
	@PostConstruct
	public void init() {
		
		if(StringUtils.isNotBlank(path))
		{
			pageForCard = pageManager.getPage(path);
		}
	}
	
	public ArticleAuthorPojo getPojo() {
		return author;
	}
	public String getAuthor() {
		Resource resource;
		if(pageForCard.hasContent()) {
			resource = pageForCard.getContentResource();
			this.getChildren(resource);
			if(authorExists) {
				return author.getAuthor();
			}
		}
		return sinAutorDefault;
	}
	
	private void getChildren(Resource resource) {
		if(resource.hasChildren()) {
			Iterable<Resource> iterable =resource.getChildren();
			for(Resource res : iterable) {
				getChildren(res);
				//log.info("***RESOURCE NAME " + res.getName());
				if(res.getResourceType().equals("enterAEM/components/content/articleAuthor")) {
					authorExists = true;
					ValueMap values = res.getValueMap();
					author = new ArticleAuthorPojo();
					author.setAuthor(values.get("articleAuthor").toString());
				}
			}
		}
	}
	
	
	
	public String getTitle() {
		return pageForCard.getTitle();
	}
	public String getPath() {
		return path;
	}
	
	public String getImageResource() {
		Resource imageRes = pageForCard.getContentResource(IMAGE_LOCATION);
		if(imageRes!=null) {
			Image image = modelFactory.getModelFromWrappedRequest(request, imageRes, Image.class);
			if(image!=null) {
				return image.getSrc();
			}
		}
		return null;
	}
	
	public String getDescription() {
		if(pageForCard!=null) {
			return pageForCard.getDescription();
		}
		return "No description";
	}
	public boolean isEmpty() {
		if(pageForCard==null) {
			return true;
		}
		return false;
	}
	
	public boolean getAuthorExists(){
		log.info(authorExists.toString());
		return authorExists;
	}
	
}
