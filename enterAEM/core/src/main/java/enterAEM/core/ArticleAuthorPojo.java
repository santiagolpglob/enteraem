package enterAEM.core;

public class ArticleAuthorPojo {

	private String author;

	private boolean exists;
	
	

	public boolean isExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
}
